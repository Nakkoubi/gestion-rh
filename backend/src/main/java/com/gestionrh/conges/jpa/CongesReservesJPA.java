package com.gestionrh.conges.jpa;

import java.util.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gestionrh.conges.model.CongesReserves;


@Repository 
public interface CongesReservesJPA extends CrudRepository<CongesReserves, Long> {

	  @Query("SELECT c FROM CongesReserves c WHERE c.date_debut > :date_debut AND c.date_fin < :date_fin")
	  Iterable<CongesReserves> findByDate(@Param("date_debut") Date debut, @Param("date_fin") Date fin);
	  
	  @Query("SELECT c FROM CongesReserves c WHERE c.collaborateur.id = :collaborateur_id")
	  Iterable<CongesReserves> findByCollaborateurId(@Param("collaborateur_id") Long collaborateur_id);
}

