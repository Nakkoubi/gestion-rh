package com.gestionrh.conges.jpa;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gestionrh.conges.model.Collaborateur;


@Repository 
public interface CollaborateurJPA extends CrudRepository<Collaborateur, Long> {

	 @Query("SELECT c FROM Collaborateur c WHERE c.username = :username")
	 public Collaborateur findCollaborateurByUsername(@Param("username") String username);
}

