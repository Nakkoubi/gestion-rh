package com.gestionrh.conges.jpa;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.gestionrh.conges.model.Role;

@Repository
public interface RoleJPA extends CrudRepository<Role, Long> {

	@Query("SELECT role\r\n"
			+ "FROM Role role\r\n"
			+ "LEFT JOIN Collaborateur collab \r\n"
			+ "WHERE role.id in collab.roles "
			+ "AND collab.id = :collaborateur_id")
	Set<Role> findRolesByCollaborateurId(@Param("collaborateur_id") Long collaborateur_id);

}
