package com.gestionrh.conges;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.gestionrh.conges.security.JWTAuthorizationFilter;

@SpringBootApplication
public class CongesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CongesApplication.class, args);
	}

	/**
	 * Sous classe pour la gestion de la sécurité
	 * @author nakkoubif
	 *
	 */
	@EnableWebSecurity
	@EnableGlobalMethodSecurity(prePostEnabled = true)
	@Configuration
	class WebSecurityConfig extends WebSecurityConfigurerAdapter {

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable()
				.addFilterAfter(new JWTAuthorizationFilter(), UsernamePasswordAuthenticationFilter.class)
				.authorizeRequests()
				.antMatchers(HttpMethod.POST, "/get_token").permitAll()
				.antMatchers(HttpMethod.POST, "/conges_reserves/search_by_date").permitAll()
				.antMatchers(HttpMethod.POST).authenticated()
				.antMatchers(HttpMethod.PUT).authenticated()
				.antMatchers(HttpMethod.DELETE).authenticated();
		}
		
		/**
		 * Console H2 non concernée par les règles de sécurités définies dans configure
		 */
		@Override
		public void configure(WebSecurity web) throws Exception {
			web
            .ignoring()
            .antMatchers("/h2-console/**");
		}
	}
}
