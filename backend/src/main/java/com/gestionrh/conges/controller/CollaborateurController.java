package com.gestionrh.conges.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gestionrh.conges.model.Collaborateur;
import com.gestionrh.conges.model.Role;
import com.gestionrh.conges.model.UserDTO;
import com.gestionrh.conges.security.JWTAuthorizationFilter;
import com.gestionrh.conges.service.CollaborateurService;
import com.gestionrh.conges.utils.Messages;
import com.gestionrh.conges.utils.ProfilsConstants;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


@RestController
public class CollaborateurController {

	@Autowired
	private CollaborateurService collaborateurService;
	
	@PostMapping("/get_token")
	public UserDTO login(@RequestParam("username") String username, @RequestParam("password") String pwd) {
		UserDTO collaborateurWithToken = getJWTToken(username);
		return collaborateurWithToken;
		
	}

	/**
	 * Génération du JWT token, récupération des rôles
	 * @param username
	 * @return
	 */
	private UserDTO getJWTToken(String username) {
		Collaborateur collaborateur = collaborateurService.getCollaborateurByUsername(username);
		String secretKey = Messages.getString("JWT.secret.key"); 
		
		//récupération des rôles du collaborateur
		Set<Role> roles = collaborateur.getRoles();
        List<SimpleGrantedAuthority> grantedAuthorities = new ArrayList<>();
         
        for (Role role : roles) {
        	grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
        }
         
        //génération du token
		String token = Jwts
				.builder()
				.setId(Messages.getString("JWT.soft.key")) 
				.setSubject(username)
				.claim(Messages.getString("JWT.authorities"), 
						grantedAuthorities.stream()
								.map(GrantedAuthority::getAuthority)
								.collect(Collectors.toList()))
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000))
				.signWith(SignatureAlgorithm.HS512,
						secretKey.getBytes()).compact();

		//mapping de la réponse
		UserDTO userDTO = new UserDTO();
		userDTO.setUsername(collaborateur.getUsername());
		userDTO.setToken(Messages.getString("JWT.bearer") + token); 
		userDTO.setRole(roles);
		
		return userDTO;
	}
	
	/**
	 * Add a new collaborateur
	 * @param collaborateur An object collaborateur
	 * @return The collaborateur object saved
	 * @throws Exception 
	 */
	@PostMapping("/collaborateur")
	public Collaborateur createCollaborateur(@RequestBody Collaborateur collaborateur) throws Exception {
		if (JWTAuthorizationFilter.hasRole(ProfilsConstants.DEVELOPER)) {
			return collaborateurService.saveCollaborateur(collaborateur);
		}else {
			throw new Exception(Messages.getString("exception.collaborateur.create.only.developer")); 
		}
	}
	
	
	/**
	 * Get one collaborateur 
	 * @param id The id of the collaborateur
	 * @return An Collaborateur object full filled
	 */
	@GetMapping("/collaborateur/{id}")
	public Collaborateur getCollaborateur(@PathVariable("id") final Long id) {
		Optional<Collaborateur> collaborateur = collaborateurService.getCollaborateur(id);
		if(collaborateur.isPresent()) {
			return collaborateur.get();
		} else {
			return null;
		}
	}
	
	/**
	 * Read - Get all collaborateurs
	 * @return - An Iterable object of Collaborateur full filled
	 */
	@GetMapping("/collaborateurs")
	public Iterable<Collaborateur> getCollaborateurs() {
		return collaborateurService.getCollaborateurs();
	}
	
	/**
	 * Update - Update an existing collaborateur
	 * @param id - The id of the collaborateur to update
	 * @param collaborateur - The collaborateur object updated
	 * @return
	 * @throws Exception 
	 */
	@PutMapping("/collaborateur/{id}")
	public Collaborateur updateCollaborateur(@PathVariable("id") final Long id, @RequestBody Collaborateur collaborateur) throws Exception {
		if (JWTAuthorizationFilter.hasRole(ProfilsConstants.DEVELOPER)) {
			Optional<Collaborateur> tmpCollab = collaborateurService.getCollaborateur(id);
			if(tmpCollab.isPresent()) {
				Collaborateur currentCollaborateur = tmpCollab.get();
				String username = collaborateur.getUsername();
				if(username != null) {
					currentCollaborateur.setUsername(username);
				}
				String prenom = collaborateur.getPrenom();
				if(prenom != null) {
					currentCollaborateur.setPrenom(prenom);
				}
				String nom = collaborateur.getNom();
				if(nom != null) {
					currentCollaborateur.setNom(nom);;
				}
				String mail = collaborateur.getMail();
				if(mail != null) {
					currentCollaborateur.setMail(mail);
				}
				String motDePasse = collaborateur.getMotDePasse();
				if(motDePasse != null) {
					currentCollaborateur.setMotDePasse(motDePasse);
				}
				Set<Role> roles = collaborateur.getRoles();
				if(!roles.isEmpty()) {
					currentCollaborateur.setRoles(roles);
				}
				collaborateurService.saveCollaborateur(currentCollaborateur);
				return currentCollaborateur;
			} else {
				return null;
			}
		}else {
			throw new Exception(Messages.getString("exception.collaborateur.update.only.developer")); 
		}
	}
	
	
	/**
	 * Delete - Delete an collaborateur
	 * @param id - The id of the collaborateur to delete
	 * @throws Exception 
	 */
	@DeleteMapping("/collaborateur/{id}")
	public String deleteCollaborateur(@PathVariable("id") final Long id) throws Exception {
		if (JWTAuthorizationFilter.hasRole(ProfilsConstants.DEVELOPER)) {
			collaborateurService.deleteCollaborateur(id);
			return Messages.getString("message.collaborateur.deleted"); 
		}else {
			throw new Exception(Messages.getString("exception.collaborateur.delete.only.developer")); 
		}
	}
}
