package com.gestionrh.conges.controller;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.gestionrh.conges.model.Collaborateur;
import com.gestionrh.conges.model.CongesReserves;
import com.gestionrh.conges.security.JWTAuthorizationFilter;
import com.gestionrh.conges.service.CollaborateurService;
import com.gestionrh.conges.service.CongesReservesService;
import com.gestionrh.conges.utils.Messages;
import com.gestionrh.conges.utils.ProfilsConstants;

@RestController
public class CongesReservesController {

	@Autowired
	private CongesReservesService congesReservesService;
	
	@Autowired
	private CollaborateurService collaborateurService;
	
	/**
	 * Crée un congé réservé
	 * @param collaborateur An object collaborateur
	 * @return The collaborateur object saved
	 * @throws Exception 
	 */
	@PostMapping("/conges_reserves/create")
	public CongesReserves addCongesReserves(@RequestBody CongesReserves congesReserves) throws Exception {
		if (JWTAuthorizationFilter.hasRole(ProfilsConstants.DEVELOPER)) {
			//Récupération du collaborateur associé à la réservation de congés
			Optional<Collaborateur> tmpCollaborateur = collaborateurService.getCollaborateur(congesReserves.getCollaborateur().getId());
			
			if(tmpCollaborateur.isPresent()) {
				Collaborateur currentCollaborateur = tmpCollaborateur.get();
				congesReserves.setCollaborateur(currentCollaborateur);
				
				//Enregistrement de la réservation de congés
				return congesReservesService.saveCongesReserves(congesReserves);
			}
		}else {
			throw new Exception(Messages.getString("exception.congesreserves.create.only.developer")); 
		}
		
		return null;
	}
	
	/**
	 * Modifie un congé réservé
	 * @param collaborateur An object collaborateur
	 * @return The collaborateur object saved
	 * @throws Exception 
	 */
	@PutMapping("/conges_reserves/update/{id}")
	public CongesReserves updateCongesReserves(@PathVariable("id") final Long id, @RequestBody CongesReserves congesReserves) throws Exception {
		if (JWTAuthorizationFilter.hasRole(ProfilsConstants.DEVELOPER)) {
			Optional<CongesReserves> tmpConge = congesReservesService.getCongesReservesById(id);
			if(tmpConge.isPresent()) {
				CongesReserves currentConge = tmpConge.get();
				
				Date date_debut = congesReserves.getDate_debut();
				if(date_debut != null) {
					currentConge.setDate_debut(date_debut);
				}
				Date date_fin = congesReserves.getDate_fin();
				if(date_debut != null) {
					currentConge.setDate_fin(date_fin);
				}
				Collaborateur collaborateur = congesReserves.getCollaborateur();
				if(collaborateur != null) {
					currentConge.setCollaborateur(collaborateur);
				}
				congesReservesService.saveCongesReserves(currentConge);
				return currentConge;
			} else {
				return null;
			}
		}else {
			throw new Exception(Messages.getString("exception.congesreserves.update.only.developer")); 
		}
		
		
	}
	
	/**
	 * Récupère tous les congés réservés 
	 * @return tous les congés réservés
	 */
	@GetMapping("/conges_reserves")
	public Iterable<CongesReserves> getCongesReserves() {
		return congesReservesService.getCongesReserves();
	}
	
	/**
	 * Récupère une réservation de congé 
	 * @return tous les congés réservés
	 */
	@GetMapping("/conges_reserves/{id}")
	public Optional<CongesReserves> getCongesReservesById(@PathVariable("id") final Long id) {
		return congesReservesService.getCongesReservesById(id);
	}
	
	/**
	 * Récupère les congés réservés d'un collaborateur 
	 * @param collaborateurId l'id collaborateur
	 * @return les congés réservés du collaborateur
	 */
	@GetMapping("/conges_reserves/collaborateur/{collaborateur_id}")
	public Iterable<CongesReserves> getCongesReservesByCollaborateurId(@PathVariable("collaborateur_id") final Long collaborateurId) {
		return congesReservesService.getCongesReservesByCollaborateurId(collaborateurId);
	}
	
	/**
	 * Get all collaborateurs
	 * @return - An Iterable object of CongesReserves full filled
	 */
	@PostMapping("/conges_reserves/search_by_date")
	public Iterable<CongesReserves> getCongesReservesByDate(@RequestParam("date_debut") @DateTimeFormat(pattern = "yyyy-MM-dd")  Date dateDebut,
			@RequestParam("date_fin") @DateTimeFormat(pattern = "yyyy-MM-dd")  Date dateFin) {
		return congesReservesService.getCongesReservesByDate(dateDebut, dateFin);
	}
	
	/**
	 * Delete an collaborateur
	 * @param id - The id of the collaborateur to delete
	 * @throws Exception 
	 */
	@DeleteMapping("/conges_reserves/delete/{id}")
	public String deleteCongesReserves(@PathVariable("id") final Long id) throws Exception {
		if (JWTAuthorizationFilter.hasRole(ProfilsConstants.DEVELOPER)) {
				congesReservesService.deleteCongesReserves(id);
				return "Réservation de congé supprimée";
		} else {
			throw new Exception(Messages.getString("exception.congesreserves.delete.only.developer")); 
		}
	}
	

}
