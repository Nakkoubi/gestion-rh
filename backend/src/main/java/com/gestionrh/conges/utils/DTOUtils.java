package com.gestionrh.conges.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Classe utilitaire pour la manipulation des données à transférer
 * @author nakkoubif
 *
 */
public class DTOUtils {
	
	public static String asJsonString(final Object obj) {
	    try {
	        return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}

}
