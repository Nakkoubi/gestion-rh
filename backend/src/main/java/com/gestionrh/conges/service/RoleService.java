package com.gestionrh.conges.service;

import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gestionrh.conges.jpa.RoleJPA;
import com.gestionrh.conges.model.Role;

@Service
public class RoleService {

	@Autowired
	RoleJPA roleJPA;
	
	/**
	 * get role by id
	 * @param id
	 * @return
	 */
	@Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.NOT_SUPPORTED)
	public Optional<Role> getRoleById(final Long id) {
		return roleJPA.findById(id);
	}
	
	/**
	 * get roles by collaborateur id
	 * @param collaborateur id
	 * @return
	 */
	@Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.NOT_SUPPORTED)
	public Set<Role> getRoleByCollaborateurId(final Long collaborateur_id) {
		return roleJPA.findRolesByCollaborateurId(collaborateur_id);
	}
	
}
