package com.gestionrh.conges.service;

import java.util.Optional;

import javax.annotation.security.RolesAllowed;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gestionrh.conges.jpa.CollaborateurJPA;
import com.gestionrh.conges.model.Collaborateur;
import com.gestionrh.conges.utils.ProfilsConstants;


@Service
public class CollaborateurService {

	@Autowired
	private CollaborateurJPA collaborateurJPA;

	/**
	 * get collaborateur by id
	 * @param id
	 * @return
	 */
	@Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.NOT_SUPPORTED)
	public Optional<Collaborateur> getCollaborateur(final Long id) {
		return collaborateurJPA.findById(id);
	}

	/**
	 * get all collaborateurs
	 * @return
	 */
	@Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.NOT_SUPPORTED)
	public Iterable<Collaborateur> getCollaborateurs() {
		return collaborateurJPA.findAll();
	}

	/**
	 * delete collaborateur by id
	 * @param id
	 */
	@Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.REQUIRES_NEW)
	public void deleteCollaborateur(final Long id) {
		collaborateurJPA.deleteById(id);
	}

	/**
	 * save collaborateur
	 * @param Collaborateur
	 * @return
	 */
	@Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.REQUIRES_NEW)
	public Collaborateur saveCollaborateur(Collaborateur Collaborateur) {
		Collaborateur savedCollaborateur = collaborateurJPA.save(Collaborateur);
		return savedCollaborateur;
	}

	/**
	 * get collaborateur by username
	 * @param username
	 * @return
	 */
	@Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.NOT_SUPPORTED)
	public Collaborateur getCollaborateurByUsername(final String username) {
		return collaborateurJPA.findCollaborateurByUsername(username);
	}
}
