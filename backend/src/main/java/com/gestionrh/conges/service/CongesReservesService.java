package com.gestionrh.conges.service;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.gestionrh.conges.jpa.CongesReservesJPA;
import com.gestionrh.conges.model.CongesReserves;


@Service
public class CongesReservesService {

	@Autowired
	private CongesReservesJPA congesReservesJPA;
	
	/**
	 * Récupère tous les congés réservés d'un collaborateur
	 * @param id collaborateur
	 * @return liste des congés réservés
	 */
	@Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.NOT_SUPPORTED)
	public Iterable<CongesReserves> getCongesReservesByCollaborateurId(final Long id) {
		return congesReservesJPA.findByCollaborateurId(id);
	}

	/**
	 * Récupère tous les congés réservés sur une plage de date
	 * @param debutConges
	 * @param finConges
	 * @return liste des congés pris sur la plage de date en paramètres
	 */
	@Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.NOT_SUPPORTED)
	public Iterable<CongesReserves> getCongesReservesByDate(final Date debutConges, final Date finConges) {
		return congesReservesJPA.findByDate(debutConges,finConges);
	}

	/**
	 * Supprime une réservation de congés d'un collaborateur
	 * @param congesReserves : la réservation à supprimer
	 */
	@Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.REQUIRES_NEW)
	public void deleteCongesReserves(final Long id) {
		congesReservesJPA.deleteById(id);
	}

	/**
	 * Crée une réservation de congés
	 * @param congesReserves
	 * @return
	 */
	@Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.REQUIRES_NEW)
	public CongesReserves saveCongesReserves(CongesReserves congesReserves) {
		CongesReserves savedCongesReserves = congesReservesJPA.save(congesReserves);
		return savedCongesReserves;
	}

	/**
	 * Recupère une reservation de congé via son id
	 * @param id de la reservation de congé
	 * @return la reservation de congé correspondant à l'id en paramètre
	 */
	@Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.NOT_SUPPORTED)
	public Optional<CongesReserves> getCongesReservesById(Long id) {
		return congesReservesJPA.findById(id);
	}

	/** 
	 * Récupère tous les congés réservés
	 * @return
	 */	
	@Transactional(isolation = Isolation.DEFAULT, propagation = Propagation.NOT_SUPPORTED)
	public Iterable<CongesReserves> getCongesReserves() {
		return congesReservesJPA.findAll();
	}

}
