package com.gestionrh.conges.model;

import java.util.Set;

public class UserDTO {

	private String username;
	private String token;
	private Set<Role> role;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public Set<Role> getRole() {
		return role;
	}
	public void setRole(Set<Role> set) {
		this.role = set;
	}
	
	
}
