drop all objects;
 
-- Table collaborateurs --
CREATE TABLE collaborateurs (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  username VARCHAR(250) NOT NULL,
  prenom VARCHAR(250) NOT NULL,
  nom VARCHAR(250) NOT NULL,
  mail VARCHAR(250) NOT NULL,
  mdp VARCHAR(250) NOT NULL,
  UNIQUE KEY username_unique (username)
);
 
INSERT INTO collaborateurs (username, prenom, nom, mail, mdp) VALUES
  ('Toto', 'Toto', 'A', 'totoa@mail.com', 'toto'),
  ('Titi', 'Titi', 'B', 'titib@mail.com', 'titi'),
  ('Tutu', 'Tutu', 'C', 'tutu@mail.com', 'tutu');
  
 
-- Table congés réservés --
CREATE TABLE conges_reserves (
  id INT AUTO_INCREMENT PRIMARY KEY,
  collaborateur_id INT,
  date_debut DATE,
  date_fin DATE,
  foreign key (collaborateur_id) references collaborateurs(id) ON DELETE CASCADE
);

INSERT INTO conges_reserves (collaborateur_id, date_debut, date_fin) VALUES
  (1,'2021-07-12','2021-07-21'),
  (2,'2021-08-12','2021-08-21'),
  (3,'2021-09-12','2021-09-21');

-- table rôle --
create table role (
	id int auto_increment PRIMARY KEY, 
	description varchar(250), 
	name varchar(250)
);

INSERT INTO role (id, description, name) VALUES (1, 'Developer role', 'DEVELOPER');
INSERT INTO role (id, description, name) VALUES (2, 'User role', 'USER');

-- table lien collaborateur<->role --
create table collaborateur_roles (
	 collaborateur_id INT not null, 
	 role_id INT not null, 
 primary key (collaborateur_id, role_id));
 
INSERT INTO collaborateur_roles (collaborateur_id, role_id) VALUES (1,1);
INSERT INTO collaborateur_roles (collaborateur_id, role_id) VALUES (1, 2);
INSERT INTO collaborateur_roles (collaborateur_id, role_id) VALUES (2, 2);
INSERT INTO collaborateur_roles (collaborateur_id, role_id) VALUES (3, 2);