/**
 * 
 */
package com.gestionrh.conges.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import com.gestionrh.conges.model.Collaborateur;
import com.gestionrh.conges.model.CongesReserves;
import com.gestionrh.conges.utils.DTOUtils;

/**
 * @author nakkoubif
 *
 */
@SpringBootTest
@AutoConfigureMockMvc
class CongesReservesControllerTest {

    Logger logger = LoggerFactory.getLogger(CongesReservesControllerTest.class);
	
    @Autowired
	public MockMvc mockMvc;
	
	@Test
	void testCreateCongesReserves() throws Exception {
		//création de la date
		Date aujourdhui = new Date();
		SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
	    
		//création des objets
		Collaborateur collaborateur = new Collaborateur();
		collaborateur.setId(1L);
		CongesReserves congesReserves = new CongesReserves();
		congesReserves.setCollaborateur(collaborateur);
		congesReserves.setDate_debut(aujourdhui);
		congesReserves.setDate_fin(aujourdhui);
		
		//envoi de la requete et vérification de la réponse
		mockMvc.perform(post("/conges_reserves/create")
				.content(DTOUtils.asJsonString(congesReserves))
			    .contentType(MediaType.APPLICATION_JSON)
			    .accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is(403));

	}
	
	@Test
	void testDeleteCongesReserves() throws Exception {
		mockMvc.perform(delete("/conges_reserves/delete/3")).andExpect(status().is(403));
	}
	
	@Test
	void getCongesReservesByCollaborateurId() throws Exception {
		mockMvc.perform(get("/conges_reserves/collaborateur/1")).andExpect(status().is(200));
	}
	
	@Test
	void getCongesReservesByDate() throws Exception {
		 String debut = "2021-07-12";
		    String fin = "2021-07-21";

		    final ResultActions result = mockMvc.perform(post("/conges_reserves/search_by_date")
					.param("date_debut", debut)
					.param("date_fin", fin)
				    .contentType(MediaType.APPLICATION_JSON)
				    .accept(MediaType.APPLICATION_JSON));
		    result.andExpect(status().is2xxSuccessful());
	}
	
}
