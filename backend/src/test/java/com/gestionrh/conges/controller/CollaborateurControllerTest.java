/**
 * 
 */
package com.gestionrh.conges.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.gestionrh.conges.model.Collaborateur;
import com.gestionrh.conges.utils.DTOUtils;

/**
 * Classe test de la classe CollaborateurController
 * @author nakkoubif
 *
 */
@SpringBootTest
@AutoConfigureMockMvc
public class CollaborateurControllerTest {

	@Autowired
	public MockMvc mockMvc;
	
	/**
	 * Test method for {@link com.gestionrh.conges.controller.CollaborateurController#createCollaborateur(com.gestionrh.conges.model.Collaborateur)}.
	 * @throws Exception 
	 */
	@Test
	void testCreateCollaborateur() throws Exception {
		Collaborateur collaborateur = new Collaborateur();
		collaborateur.setUsername("test");
		collaborateur.setMail("test");
		collaborateur.setMotDePasse("test");
		collaborateur.setNom("test");
		collaborateur.setPrenom("test");
		
		mockMvc.perform(post("/collaborateur")
				.content(DTOUtils.asJsonString(collaborateur))
			    .contentType(MediaType.APPLICATION_JSON)
			    .accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is(403));
	}

	/**
	 * Test method for {@link com.gestionrh.conges.controller.CollaborateurController#getCollaborateur(java.lang.Long)}.
	 * @throws Exception 
	 */
	@Test
	void testGetCollaborateur() throws Exception {
		mockMvc.perform(get("/collaborateur/1")).andExpect(status().is2xxSuccessful());
	}

	/**
	 * Test method for {@link com.gestionrh.conges.controller.CollaborateurController#getCollaborateurs()}.
	 * @throws Exception 
	 */
	@Test
	void testGetCollaborateurs() throws Exception {
		mockMvc.perform(get("/collaborateurs")).andExpect(status().isOk());
	}

	/**
	 * Test method for {@link com.gestionrh.conges.controller.CollaborateurController#updateCollaborateur(java.lang.Long, com.gestionrh.conges.model.Collaborateur)}.
	 * @throws Exception 
	 */
	@Test
	void testUpdateCollaborateur() throws Exception {
		Collaborateur collaborateur = new Collaborateur();
		collaborateur.setMail("newmail@mail.com");
		
		mockMvc.perform(put("/collaborateur/{id}", 1)
				.content(DTOUtils.asJsonString(collaborateur))
			    .contentType(MediaType.APPLICATION_JSON)
			    .accept(MediaType.APPLICATION_JSON))
		.andExpect(status().is(403));
	}

	/**
	 * Test method for {@link com.gestionrh.conges.controller.CollaborateurController#deleteCollaborateur(java.lang.Long)}.
	 * @throws Exception 
	 */
	@Test
	void testDeleteCollaborateur() throws Exception {
		mockMvc.perform(delete("/collaborateur/1")).andExpect(status().is(403));
	}


}
