# Gestion RH

API Rest pour l'ajout, la modification et la suppression de collaborateurs et de congés <br>
<br>

# Informations API

Port : 9000 <br>
Context root : /gestion-rh <br>
<br>

# Démarrage

Ouvrir Spring Tool Suite 4 <br>
Importer le projet maven <br>
Cliquer sur Start dans le Boot Dashboard <br>

Ouvrir Postman <br>
Importer le fichier src/main/resources/gestionRH.postman_collection.json <br>
<br>

# Sécurité

Pour générer un token, appeler l'API http://localhost:9000/gestion-rh/get_token <br>
Fichier Postman présent sous : src/main/resources. <br>

# Console H2

URL : http://localhost:9000/gestion-rh/h2-console <br>
URL JDBC : jdbc:h2:mem:testdb <br>
User/mdp par défaut <br>
<br>

